import os, sys
import json
import networkx as nx
import bct
import numpy as np
import pickle
import graph_tool.all as gt
import matplotlib.pyplot as plt
from sqlitedict import SqliteDict
import utils_network
import utils_networkx
import utils_gt
import utils_network_metrics
import utils_filtration_metrics

def filter_session_country(session, country_code):
    """
    Filter sessions in which all pageviews come from the same country (specified via country_code).
    Returns True or False
    """
    keep_session = False
    session_country_list = list(set([h["country_code"] for h in session]))
    if session_country_list == [country_code]:
        keep_session = True
    return keep_session

def main(suffix):

    snapshot = "2022-03"
    wiki_db = "enwiki"
    country_code_sel = "US" # country filter
    N_sample = 1000 # number of samples; not used, but included for reference

    mode = "sqlite" 

    FNAME_read = "/home/mgerlach/REPOS/curios-critical-readers/data/sessions-app_%s_%s_small%s.json"%(wiki_db, snapshot, suffix)    
    n_processed = 0
    n_filter = 0
    list_sessions = []
    with open(FNAME_read) as fin:
        for line in fin:
            json_in = json.loads(line)
            n_processed += 1
            session = json_in.get("session", [])
            list_sessions += [session]

    # load a links table of the form {page_id: pageids of outlinks}
    FNAME_read = "/home/mgerlach/REPOS/curios-critical-readers/data/pages-links_%s_%s.{0}"%(wiki_db, snapshot)
    if mode == "pickle":
        with open(FNAME_read.format("pkl"), "rb") as fin:
            dict_links = pickle.load(fin)  
    elif mode == "sqlite":
        dict_links = SqliteDict(FNAME_read.format("sqlite"))
    else:
        dict_links = {}

    # loop through the networks and analyze them
    directed = False # only consider undirected networks
    timed = True
    all_dict = {}
    num_iters = 5 # number of re-wirings of each network
    db = SqliteDict("/srv/home/shubhankar/04272022/filtration_metrics_small%s.sqlite" % suffix)

    for i in range(n_processed):
        
        if i % 50 == 0:
            print(i)
        
        list_nodes, list_edges = utils_network.session2edgelist_links(list_sessions[i], 
                                                                      dict_links, 
                                                                      directed = directed)
        G = utils_networkx.make_graph_links(list_nodes, list_edges, directed = directed)
        A = nx.adjacency_matrix(G).todense()
        weighted_A = utils_filtration_metrics.make_filtration_matrix(A)
        bars = utils_filtration_metrics.get_barcode(weighted_A)
        bettis = utils_filtration_metrics.betti_curves(bars, weighted_A.shape[0])
        # MDL = utils_filtration_metrics.compute_filtration_MDL(A)
        [d, conform] = utils_filtration_metrics.compute_mechanical_features(A)
        
        subj_dict = {}
        subj_dict['n'] = A.shape[0]
        subj_dict['betti_0'] = bettis[0]
        subj_dict['betti_1'] = bettis[1]
        # subj_dict['betti_2'] = bettis[2]
        # subj_dict['MDL'] = MDL
        subj_dict['d'] = d
        subj_dict['conform'] = conform
        
        betti_0_null = np.zeros((num_iters, len(bettis[0])))
        betti_1_null = np.zeros((num_iters, len(bettis[1])))
        # betti_2_null = np.zeros((num_iters, len(bettis[2])))
        # MDL_null = np.zeros((num_iters, len(MDL)))
        d_null = np.zeros((num_iters, len(d)))
        conform_null = np.zeros((num_iters, len(conform)))
        
        for j in range(num_iters):
            list_edges_rewired = utils_network.rewire_edges(list_edges, directed = directed)
            G = utils_networkx.make_graph_links(list_nodes, list_edges_rewired, directed = directed)
            A = nx.adjacency_matrix(G).todense()
            weighted_A = utils_filtration_metrics.make_filtration_matrix(A)
            bars_null = utils_filtration_metrics.get_barcode(weighted_A)
            bettis_null = utils_filtration_metrics.betti_curves(bars_null, weighted_A.shape[0])
            betti_0_null[j, ] = bettis_null[0]
            betti_1_null[j, ] = bettis_null[1]
            # betti_2_null[j, ] = bettis_null[2]
            # MDL_null[j, ] = utils_filtration_metrics.compute_filtration_MDL(A)
            [d_null[j, ], conform_null[j, ]] = utils_filtration_metrics.compute_mechanical_features(A)
            
        subj_dict['null_iters'] = num_iters
        
        subj_dict['betti_0_null_mean'] = np.nanmean(betti_0_null, 0)
        subj_dict['betti_1_null_mean'] = np.nanmean(betti_1_null, 0)
        # subj_dict['betti_2_null_mean'] = np.nanmean(betti_2_null, 0)
        # subj_dict['MDL_null_mean'] = np.nanmean(MDL_null, 0)
        subj_dict['d_null_mean'] = np.nanmean(d_null, 0)
        subj_dict['conform_null_mean'] = np.nanmean(conform_null, 0)
        
        subj_dict['betti_0_null_std'] = np.nanstd(betti_0_null, 0)
        subj_dict['betti_1_null_std'] = np.nanstd(betti_1_null, 0)
        # subj_dict['betti_2_null_std'] = np.nanstd(betti_2_null, 0)
        # subj_dict['MDL_null_std'] = np.nanstd(MDL_null, 0)
        subj_dict['d_null_std'] = np.nanstd(d_null, 0)
        subj_dict['conform_null_std'] = np.nanstd(conform_null, 0)
        
        db[i] = subj_dict
        db.commit()
        
    db.close()
    
    return 0

if __name__ == "__main__":
    if len(sys.argv) > 1: # "v2" or future alternatives
        suffix = sys.argv[1]
        suffix = "_" + suffix
        sys.exit(main(suffix))
    else: # corresponds to ""
        suffix = ""
        sys.exit(main(suffix))
        
        