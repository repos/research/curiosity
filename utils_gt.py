# https://github.com/epfl-dlab/wikinav-approx/blob/main/code/tokenizer.py
import graph_tool as gt
from collections import defaultdict

def make_graph_transitions(list_nodes, list_edges):
    """
    Generate a graph-object in graph-tool from the list of edges and nodes.
    """
    g = gt.Graph(directed=True)
    v_add = defaultdict(lambda: g.add_vertex())

    # vertex and edge properties
    vtitle = g.vp["page_title"] = g.new_vp("string")
    vpid = g.vp["page_id"] = g.new_vp("int")
    ets = g.ep["ts"] = g.new_ep("int")

    # add nodes
    for node in list_nodes:
        node_i = node["index"]
        node_pid = node.get("page_id",-1)
        node_ptitle = node.get("page_title","n/a")
        v = v_add[node_i]
        vtitle[v] = node_ptitle
        vpid[v] = node_pid

    for edge in list_edges:
        edge_s = edge[0]
        edge_t = edge[1]
        vs = v_add[edge_s]
        vt = v_add[edge_t]
        e = g.add_edge(vs, vt)
        edge_ts = edge[2]
        ets[e] = edge_ts
    return g

def make_graph_links(list_nodes, list_edges, directed = True):
    """
    Generate a graph-object in graph-tool from the list of edges and nodes.
    """
    g = gt.Graph(directed=True)
    v_add = defaultdict(lambda: g.add_vertex())

    # vertex and edge properties
    vtitle = g.vp["page_title"] = g.new_vp("string")
    vpid = g.vp["page_id"] = g.new_vp("int")

    # add nodes
    for node in list_nodes:
        node_i = node["index"]
        node_pid = node.get("page_id",-1)
        node_ptitle = node.get("page_title","n/a")
        v = v_add[node_i]
        vtitle[v] = node_ptitle
        vpid[v] = node_pid
        
    for edge in list_edges:
        edge_s = edge[0]
        edge_t = edge[1]
        vs = v_add[edge_s]
        vt = v_add[edge_t]
        e = g.add_edge(vs, vt)
    if directed == False:
        g.set_directed(False)
    return g

def make_graph_similarity(list_nodes, list_edges, normalized_weights=None):
    """
    Generate a graph-object in graph-tool from the list of edges and nodes.
    """
    g = gt.Graph(directed=False)
    v_add = defaultdict(lambda: g.add_vertex())

    # vertex and edge properties
    vtitle = g.vp["page_title"] = g.new_vp("string")
    vpid = g.vp["page_id"] = g.new_vp("int")
    eweight = g.ep["weight"] = g.new_ep("double")

    # add nodes
    for node in list_nodes:
        node_i = node["index"]
        node_pid = node.get("page_id",-1)
        node_ptitle = node.get("page_title","n/a")
        v = v_add[node_i]
        vtitle[v] = node_ptitle
        vpid[v] = node_pid
        
    for edge in list_edges:
        edge_s = edge[0]
        edge_t = edge[1]
        edge_w = edge[2]
        vs = v_add[edge_s]
        vt = v_add[edge_t]
        e = g.add_edge(vs, vt)
        if normalized_weights != None:
            e_mu, e_std = normalized_weights
            edge_w = (edge_w-e_mu)/e_std
        eweight[e] = edge_w
    return g