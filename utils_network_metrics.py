import networkx as nx
import bct
import numpy as np
import graph_tool.all as gt
import time
import utils_gt

def calculate_network_metrics(g, g_gt, timed):
    """
    Calculate the suite of network metrics for graph g
    g: graph object output from the function utils_networkx.make_graph_links
    g_gt: graph object output from utils_gt.make_graph_links

    timed: True or False to return run time for nx, bct, gt metrics
    
    N: Number of nodes in the network N
    rho: Density
    C: Clustering coefficient
    k: average degree
    spl: Characteristic path length
    eff: gloabal efficiency
    core-periphery structure: Borgatti-Everett method
    mdl: minimum description length
    modq: modularity
    """
    # if disconnected, take largest component
    g_conmax = g.subgraph(max(nx.connected_components(g), key=len))

    tic = time.perf_counter()    
    N = nx.number_of_nodes(g) # size
    rho = nx.density(g) # density
    C = nx.average_clustering(g) # clustering
    k = np.mean([v for k,v in nx.degree(g)]) # degree
    spl = nx.average_shortest_path_length(g_conmax) # shortest path length
    eff = nx.global_efficiency(g) # global efficiency
    toc = time.perf_counter()
    
    networkxMetrics_time = toc-tic

    # if len(g_conmax) > 4:
    #     sigma = nx.sigma(g_conmax) # small-worldness
    # else:
    #     sigma = np.nan
    
    tic = time.perf_counter()
    if len(g_conmax) > 1:
        cp = bct.core_periphery_dir(nx.convert_matrix.to_numpy_array(g))
    else:
        cp = [np.nan, np.nan]
    toc = time.perf_counter()
    
    bctMetrics_time = toc-tic
    
    tic = time.perf_counter()
    # fit a hierarchical blockmodel
    state = gt.minimize_nested_blockmodel_dl(g_gt)
    # do a few swaps to find minimum
    for i in range(100):
        ret = state.multiflip_mcmc_sweep(niter=10, beta=np.inf)
    
    # description length
    mdl = state.entropy()
    
    l = 0
    blocks = state.project_level(l).get_blocks()
    modq = gt.modularity(g_gt,blocks)
    toc = time.perf_counter()
    
    gtMetrics_time = toc-tic
    
    if timed==True:
        return [N, rho, C, k, spl, eff, np.around(cp[1],8), mdl, modq, networkxMetrics_time, bctMetrics_time, gtMetrics_time]
    else:
        return [N, rho, C, k, spl, eff, np.around(cp[1],8), mdl, modq]

def calculate_network_metrics_dict(g, g_gt, timed):
    """
    Calculate the suite of network metrics for graph g
    g: graph object output from the function utils_networkx.make_graph_links
    g_gt: graph object output from utils_gt.make_graph_links

    timed: True or False to return run time for nx, bct, gt metrics
    
    N: Number of nodes in the network N
    E: Number of edges in the network
    rho: Density
    C: Clustering coefficient
    k: average degree
    spl: Characteristic path length
    eff: gloabal efficiency
    cp: core-periphery structure: Borgatti-Everett method
    mdl: minimum description length of the hierarchical degree-corrected stochastic block model
    B: Number of blocks in the SBM on the lowest level of the hierarchy
    modq: modularity of the partition obtained from the SBM
    """
    # if disconnected, take largest component
    g_conmax = g.subgraph(max(nx.connected_components(g), key=len))

    tic = time.perf_counter()    
    N = nx.number_of_nodes(g) # nodes
    E = nx.number_of_edges(g) # edges
    rho = nx.density(g) # density
    C = nx.average_clustering(g) # clustering
    k = np.mean([v for k,v in nx.degree(g)]) # degree
    spl = nx.average_shortest_path_length(g_conmax) # shortest path length
    eff = nx.global_efficiency(g) # global efficiency
    toc = time.perf_counter()
    
    networkxMetrics_time = toc-tic
    
    tic = time.perf_counter()
    if len(g_conmax) > 1:
        cp = bct.core_periphery_dir(nx.convert_matrix.to_numpy_array(g))
    else:
        cp = [np.nan, np.nan]
    toc = time.perf_counter()
    
    bctMetrics_time = toc-tic
    
    tic = time.perf_counter()
    # fit a hierarchical blockmodel
    state = gt.minimize_nested_blockmodel_dl(g_gt)
    # do a few swaps to find minimum
    for i in range(100):
        ret = state.multiflip_mcmc_sweep(niter=10, beta=np.inf)
    
    # description length
    mdl = state.entropy()
    
    # partition on level l
    l = 0 # lowest level of the hierarchy
    blocks = state.project_level(l).get_blocks() # block-membership of each node 
    B = len(set(blocks.a)) # number of non-empty blocks
    modq = gt.modularity(g_gt,blocks) # modularity score of partition
    toc = time.perf_counter()
    
    gtMetrics_time = toc-tic
    
    out_dict = {
        "N": N,
        "E": E,
        "rho": rho,
        "C": C,
        "k": k,
        "spl": spl,
        "eff": eff,
        "cp": np.around(cp[1],8),
        "mdl": mdl,
        "B": B,
        "modq": modq
    }
    if timed==True:
        out_dict["networkxMetrics_time"] = networkxMetrics_time
        out_dict["bctMetrics_time"] = bctMetrics_time
        out_dict["gtMetrics_time"] = gtMetrics_time
    return out_dict
    

def filtration_metric_mdl(g_gt, s=1, n_mcmc=0):
    '''
    calculate the mdl as a filtration metric for the growing network.
    
    - g_gt: graph-tool graph
    - s, int: spacing of the subsampling  (optional, default=1)
    - n_mcmc, int: number of mcmc-rounds to do after minimization to find global minimum (optional, default=0): 
    '''
    N=g_gt.num_vertices()
    # spacing s
    list_N = np.arange(s,N+1,s)

    list_n=[]
    list_e=[]
    list_mdl=[]
    list_mdlr=[]
    list_time=[]

    vertex_filter=g_gt.new_vp("boolean")
    # we iterate over each node index
    for n in list_N:   
        t1 = time.time()
        # only consider the first n nodes via a vertex-filter
        g_gt.clear_filters()
        for i_n in range(n):
            vertex_filter[g_gt.vertex(i_n)]=1
        g_gt.set_vertex_filter(vertex_filter)

        # fit the SBM and get the MDL
        state = gt.minimize_nested_blockmodel_dl(g_gt)
        # do a few swaps to find minimum
        for i in range(n_mcmc):
            ret = state.multiflip_mcmc_sweep(niter=10, beta=np.inf)
        mdl = state.entropy()
        # get the MDL for the random network by forcing B=1 (number of groups)
        mdlr=gt.minimize_nested_blockmodel_dl(g_gt,multilevel_mcmc_args=dict(B_min=1, B_max=1)).entropy()

        list_n+=[g_gt.num_vertices()]
        list_e+=[g_gt.num_edges()]
        list_mdl+=[mdl]
        list_mdlr+=[mdlr]
        t2=time.time()
        list_time+=[t2-t1]
        g_gt.clear_filters()

    dict_result = {
        "nodes": list_n,
        "edges": list_e,
        "mdl": list_mdl,
        "mdl_random": list_mdlr,
        "time": list_time
    }
    return dict_result