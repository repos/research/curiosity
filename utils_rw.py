import numpy as np
import utils_network
import utils_networkx
import networkx as nx
import collections
import copy

def get_session_rw(session, dict_links, dict_ext, p_link = 0.375):
    
    arr_pid = dict_ext["arr_pid"]
    arr_pid_p = dict_ext["arr_pid_p"]
    
    # get the first step
    pos = 1
    pid_current = session[0]["page_id"]
    referer_page_id = None
    referer_class = "external"

    n_rw = len(session)

    # generate some candidate choices via prob~views
    pid_cand_ext = np.random.choice(arr_pid,size=n_rw, p=arr_pid_p)
    dict_current = {'pos':pos, 'page_id':pid_current, 'referer_page_id':referer_page_id, 'referer_class':referer_class}
    session_rw = [dict_current]

    # iterate for n_rw-1 steps to get a random walk of length n_rw
    for i_n in range(n_rw-1):
        # pick a link or not
        p_rand = np.random.random()
        if p_rand < p_link:
            # pick one of the links
            pid_cand_int = dict_links.get(pid_current,[])
            if len(pid_cand_int)>0:
                int_rand = np.random.randint(0,len(pid_cand_int))
                pid_next = int(pid_cand_int[int_rand])
                referer_page_id = int(pid_current)
                referer_class = "internal"

            else:
                # if there was no outgoing link, we pick one according to external popularity
                pid_next = int(pid_cand_ext[i_n])
                referer_page_id = None
                referer_class = "external"

        else:
            # pick a random article
            pid_next = int(pid_cand_ext[i_n])
            referer_page_id = None
            referer_class = "external"


        ## save the next step
        pos+=1
        dict_current =  {'pos':pos, 'page_id':pid_next, 'referer_page_id':referer_page_id, 'referer_class':referer_class}
        session_rw+=[dict_current]
        pid_current = pid_next
    return session_rw


def get_session_levy(session, dict_links, dict_ext, levy_c, reinforcement):
    
    # Calculating diameter per graph
    # directed=False
    # list_nodes, list_edges = utils_network.session2edgelist_links(session, dict_links, directed = directed)
    # G = utils_networkx.make_graph_links(list_nodes, list_edges, directed=directed)
    # # if disconnected, take largest component
    # g_conmax = G.subgraph(max(nx.connected_components(G), key=len))
    # diam = nx.diameter(g_conmax)
    
    # Using fixed diameter (this is maximum shortest path length across the 1000 networks)
    diam = 9
    
    numberOfSteps = len  # set max number of steps
    x = np.arange(1, diam+1, 1)  # set upper bound for step size
    pdfLevy = np.power(1/x, levy_c)  # set u value, where 1 <= u <= 3
    pdfLevy = pdfLevy/sum(pdfLevy)

    # TO DO SELECT ONLY NODES AND EDGES IN THE INDIVIDUAL'S DATA
    arr_pid = dict_ext["arr_pid"]
    arr_pid_p = dict_ext["arr_pid_p"]

    # get the first step
    pos = 1
    pid_current = session[0]["page_id"]
    pid_next = None
    referer_page_id = None
    referer_class = "external"

    n_rw = len(session)
    
    # generate some candidate choices via prob~views
    pid_cand_ext = np.random.choice(arr_pid, size=1, p=arr_pid_p)
    dict_current = {'pos':pos, 'page_id':pid_current, 'referer_page_id':referer_page_id, 'referer_class':referer_class}
    session_rw = [dict_current]

    # save edges reinforced
    pid_reinforced = []

    # iterate for n_rw-1 steps to get a random walk of length n_rw
    for i_n in range(n_rw):
        # re-define current if we're past the 1st step
        if i_n>0:
            pid_current = copy.deepcopy(pid_next)
        # random walk or pick a link if no out-links
        # sample distance of the random walk steps
        stepSize = np.random.choice(x, p=pdfLevy) # get distance for this step
        for step in range(stepSize):
            if i_n==0: # for first step of the random walk
                if step==0:
                    pid_cand_int = dict_links.get(pid_current,[]) # get possible links from starting node
                else:
                    pid_cand_int = dict_links.get(pid_next,[]) # get possible links from previous node
                if len(pid_cand_int)>0:
                    int_rand = np.random.randint(0,len(pid_cand_int))
                    pid_next = int(pid_cand_int[int_rand])
                    referer_page_id = int(pid_current)
                    referer_class = "internal"
                else:
                    # if there was no outgoing link, we pick one according to external popularity
                    pid_next = int(np.random.choice(arr_pid, size=1, p=arr_pid_p))
                    referer_page_id = None
                    referer_class = "external"
            else: # for the other steps of the random walk
                pid_cand_int = dict_links.get(pid_next,[]) # get possible links from the previous node
                if len(pid_cand_int)>0:
                    # check if edges to candidates were previously used if on a previously visited node
                    check_visit = [y for x,y in pid_reinforced if x==pid_next]
                    if len(check_visit)>0:
                        check_reinforcement = [y for y in check_visit if y in pid_cand_int]
                        if len(check_reinforcement) > 0:
                            # get frequencies of reinforced edges
                            dict_reinforced = dict(collections.Counter(check_reinforcement))
                            # multiply frequencies by reinforcement
                            dict_reinforced.update({k:v*reinforcement for k,v in dict_reinforced.items()})
                            # combine reinforced and candidate lists
                            dict_cand = dict(collections.Counter([x for x in pid_cand_int if x not in dict_reinforced.keys()]))
                            dict_reinforced.update(dict_cand)
                            # get denominator and convert frequency to prob
                            total = sum(list(dict_reinforced.values()))
                            # pick element weighted by frequency * reinforcement
                            pid_next = int(np.random.choice(list(dict_reinforced.keys()), p=[x/total for x in list(dict_reinforced.values())]))
                            referer_page_id = int(pid_current)
                            referer_class = "internal"
                        else:
                            int_rand = np.random.randint(0,len(pid_cand_int))
                            referer_page_id = int(pid_current)
                            pid_next = int(pid_cand_int[int_rand])
                            referer_class = "internal"
                    else:
                        int_rand = np.random.randint(0,len(pid_cand_int))
                        referer_page_id = int(pid_current)
                        pid_next = int(pid_cand_int[int_rand])
                        referer_class = "internal"
                else:
                    # if there was no outgoing link, we pick one according to external popularity
                    pid_next = int(np.random.choice(arr_pid, size=1, p=arr_pid_p))
                    referer_page_id = None
                    referer_class = "external"
        ## save the previous and next step
        pos+=1
        dict_current = {'pos':pos, 'page_id':pid_next, 'referer_page_id':referer_page_id, 'referer_class':referer_class}
        session_rw+=[dict_current]
        if reinforcement==0:
            pid_reinforced = [[None,None]]
        else:
            pid_reinforced.append([pid_current, pid_next])
    return session_rw