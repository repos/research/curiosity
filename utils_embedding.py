import numpy as np

def get_embedding_text(text, model, method = "fasttext"):
    embedding = None
    if method == "fasttext":
        embedding = model.get_sentence_vector(text)
        ## if the entries of the vector contain nan
        if np.isnan(embedding[0]) == True:
            embedding = None
    return embedding