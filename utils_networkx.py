import networkx as nx

def make_graph_transitions(list_nodes, list_edges):
    """
    Generate a graph-object in networkx from the list of edges and nodes.
    Multi-Directed graph. (a transition can happen more than once)
    """
    list_add_nodes = []
    for node in list_nodes:
        node_i = node["index"]
        node_pid = node["page_id"]
        node_ptitle = node["page_title"]
        list_add_nodes += [(node_i, {"page_id":node_pid, "page_title": node_ptitle})]
    list_add_edges = []
    for edge_s,edge_t,edge_t in list_edges:
        list_add_edges += [(edge_s, edge_t, {"time":edge_t})]
        
    g = nx.MultiDiGraph()
    g.add_nodes_from(list_add_nodes)
    g.add_edges_from(list_add_edges)   
    return g

def make_graph_links(list_nodes, list_edges, directed = True):
    """
    Generate a graph-object in networkx from the list of edges and nodes.
    Directed graph by default but option to turn into non-directed graph.
    """
    list_add_nodes = []
    for node in list_nodes:
        node_i = node["index"]
        node_pid = node["page_id"]
        node_ptitle = node["page_title"]
        list_add_nodes += [(node_i, {"page_id":node_pid, "page_title": node_ptitle})]
    g = nx.DiGraph()
    g.add_nodes_from(list_add_nodes)
    g.add_edges_from(list_edges)
    if directed == False:
        g = g.to_undirected()
    return g

def make_graph_similarity(list_nodes, list_edges):
    """
    Generate a graph-object in networkx from the list of edges and nodes.
    Weighted undirected graph.
    """
    list_add_nodes = []
    for node in list_nodes:
        node_i = node["index"]
        node_pid = node["page_id"]
        node_ptitle = node["page_title"]
        list_add_nodes += [(node_i, {"page_id":node_pid, "page_title": node_ptitle})]
#     list_add_edges = []
#     for edge_s,edge_t,edge_w in list_edges:
#         list_add_edges += [(edge_s, edge_t, {"weight":edge_w})]
    g = nx.Graph()
    g.add_nodes_from(list_add_nodes)
    g.add_weighted_edges_from(list_edges)
    return g