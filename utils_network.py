import numpy as np
from scipy.spatial import distance

def get_nodes(session):
    '''
    Get unique list of nodes with index, page_id, page_title.
    '''
    set_page_id = set([])
    list_nodes = []
    k = 0
    for pageload in session:
        page_id = pageload["page_id"]
        page_title = pageload.get("page_title")
        if page_id not in set_page_id:
            set_page_id.update([page_id])
            dict_node = {"index":k, "page_id":page_id, "page_title":page_title}
            list_nodes += [dict_node]
            k+=1
    return list_nodes

def session2edgelist_transitions(session):
    '''
    From a reading session get
    - list_nodes: unique list with page-ids of all nodes
    - list_edges: all direct transitions.
    '''

    list_nodes = get_nodes(session)
    list_pageids = [h["page_id"] for h in list_nodes]

    list_edges = []
    ## iterate through all pageloads and find direct transitions between nodes
    i_r = 0 # keeping track of index of transition (time-ordered)
    for pageload in session:
        page_id = pageload["page_id"]
        referer_class = pageload["referer_class"]
        if referer_class == "internal":
            referer_page_id = pageload["referer_page_id"]
            if referer_page_id in list_pageids:
                k_s = list_pageids.index(referer_page_id)
                k_t = list_pageids.index(page_id)
                edge_ts = i_r
                list_edges+=[(k_s,k_t, edge_ts)]
                i_r+=1
    return list_nodes, list_edges

def session2edgelist_links(session, dict_links, directed=True):
    '''
    From a reading session get
    - list_nodes: unique list with page-ids of all nodes
    - list_edges: all pairs of nodes (source,target) for which there is a link from source to target

    Input:
    - session, list with pageloads containing page_id
    - dict_links, dictionary containing all the outlinks of a page {page_id: list_of_page_ids_of_outlinks}
    Options:
    - keep_pids: keep the page-ids to index nodes, otherwise 0,...,N-1
    - directed: if False, consider as undirected and remove duplicate edges (default=True)
    '''

    # all unique nodes
    list_nodes = get_nodes(session)
    list_pageids = [h["page_id"] for h in list_nodes]
    
    list_edges = []
    i_page_id = 0
    for i_page_id, page_id in enumerate(list_pageids):
        ## iterate over all outlinks. add if outlink-page is in the list of nodes
        list_page_id_out = dict_links.get(page_id,[])
        for page_id_out in list_page_id_out:
            if page_id_out in list_pageids:
                k_s = i_page_id
                k_t = list_pageids.index(page_id_out)
                list_edges+=[(k_s,k_t)]
    if directed==False:
        list_edges_ud = []
        for e in list_edges:
            e_s = tuple(sorted(e))
            if e_s not in list_edges_ud:
                list_edges_ud+=[e_s]
        list_edges = list_edges_ud
    return list_nodes, list_edges

def session2edgelist_similarity(session, dict_vec, edge_threshold_min = -np.inf, edge_threshold_max = np.inf):
    '''
    From a reading session get
    - list_nodes: unique list with page-ids of all nodes
    - list_edges: cosine similarity between all pairs of nodes

    Input:
    - session, list with pageloads containing page_id
    - dict_vec, dictionary containing a vector-representation for each page (e.g. from text): {page_id: vector (list or numpy array)}

    Options:
    - thresholds for edge-weight: default is no thresholding (default: no threshold)

    '''
    list_nodes = get_nodes(session)
    list_pageids = [h["page_id"] for h in list_nodes]
    list_edges = []


    # get the embeddings for each node
    n_dim = len(next(iter(dict_vec.values())))
    X = np.zeros(( len(list_pageids), n_dim ))
    for i_page_id, page_id in enumerate(list_pageids):
        # get vector, if n/a assign nan-values
        page_vec = dict_vec.get(page_id, np.nan*np.zeros(n_dim))
        X[i_page_id,:]=page_vec

    # calculate all pairwise distances
    X_dist = distance.cdist(X,X,metric="cosine")

    # keep only unique undirected non-self edges
    for i_k1, k1 in enumerate(list_pageids):
        for i_k2,k2 in enumerate(list_pageids):
            if i_k1<i_k2:
                cos_dist = X_dist[i_k1,i_k2]
                cos_sim = np.round(1.-cos_dist,3)
                # return a tuple: page1, page2, similarity
                edge = (i_k1,i_k2,cos_sim)
                # tresholding, also removes edges with nan-values (e.g. when the node is not in the dict_vec)
                if cos_sim>=edge_threshold_min and cos_sim<=edge_threshold_max:
                    list_edges+=[edge]
    return list_nodes, list_edges

def rewire_edges(list_edges, NE = 10, directed = True):
    '''
    Return a rewired list of edges.
    We perform edge-swapping of pairs of edges. Each edge is swapped NE times with another randomly chosen edge.
    Doesnt take into account edge-weights.
    
    '''
    E = len(list_edges)
    # make a copy of the list of edges
    list_edges_rewired = list_edges.copy()
    
    # we iterate over each edge-index NE times
    for iNE in range(NE):    
        if E==1:
            continue
        # iterate over all edge indices in a random order
        list_ie1 = np.arange(E)
        np.random.shuffle(list_ie1)
        for ie1 in list_ie1:
            # pick one other edge-index randomly
            ie2 = np.random.randint(0,E-1)
            if ie2>=ie1:
                ie2+=1
            # get the two edges and their sources and targets
            e1 = list_edges_rewired[ie1]
            e2 = list_edges_rewired[ie2]
            s1,t1 = e1[:2]
            s2,t2 = e2[:2]

            # swap source and target
            e1_r = (s1,t2)
            e2_r = (s2,t1)

            # dont swap if we create self-edges
            if s1==t2 or s2==t1:
                continue
            # dont swap if the newly created edges already exist (avoid multi-edges)
            if e1_r in list_edges_rewired or e2_r in list_edges_rewired:
                continue
            # for directed graphs we also have to check whether the edge already exists the other way around
            if directed==False:
                if e1_r[::-1] in list_edges_rewired or e2_r[::-1] in list_edges_rewired:
                    continue
            # replace the edges with the swapped ones
            list_edges_rewired[ie1] = e1_r
            list_edges_rewired[ie2] = e2_r
    return list_edges_rewired